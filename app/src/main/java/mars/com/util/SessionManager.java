package mars.com.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by Harsh on 5/27/2017.
 */
public class SessionManager {
    private static String TAG = SessionManager.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;

    SharedPreferences.Editor editor;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "AndroidLogin";

    private static final String KEY_IS_TEACHER_LOGGEDIN = "isTeacherLoggedIn";
    private static final String KEY_IS_STUDENT_LOGGEDIN = "isStudentLoggedIn";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setTeacherLogin(boolean isLoggedIn) {

        editor.putBoolean(KEY_IS_TEACHER_LOGGEDIN, isLoggedIn);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public void setStudentLogin(boolean isLoggedIn) {

        editor.putBoolean(KEY_IS_STUDENT_LOGGEDIN, isLoggedIn);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public boolean isTeacherLoggedIn() {
        return pref.getBoolean(KEY_IS_TEACHER_LOGGEDIN, false);
    }


    public boolean isStudentLoggedIn() {
        return pref.getBoolean(KEY_IS_STUDENT_LOGGEDIN, false);
    }
}
