package mars.com.constants;

/**
 * Created by Harsh on 5/27/2017.
 */
public interface NetworkConstants {

    String SYLLABUS_URL = "https://view.publitas.com/digital-impact-square/cs_syllabus/";
    String ACADEMIC_CALENDAR_URL = "https://view.publitas.com/digital-impact-square/academic_calendar/";
    String NETWORK_IP = "http://192.168.137.192:8080";
    String FACULTY_ACCESS_URL = NETWORK_IP + "/facultyAccess";
    String GET_CLASS_LIST_URL = NETWORK_IP + "/students";
    String STUDENT_ACCESS_URL = NETWORK_IP + "/studentAccess";
    String ATTENDANCE_UPLOAD_URL = NETWORK_IP + "/updateAttendance";
    String MARKS_UPLOAD_URL = NETWORK_IP + "/updateMarks";
    String GET_STUDENT_ATTENDANCE_URL = NETWORK_IP + "/getAttendance";
    String GET_STUDENT_MARKS_URL = NETWORK_IP + "/getMarks";
    String ACADEMIC_CALENDAR_URL_NEW = NETWORK_IP + "/academicCalendar";
    String SYLLABUS_URL_NEW = NETWORK_IP + "/csSyllabus";
}
