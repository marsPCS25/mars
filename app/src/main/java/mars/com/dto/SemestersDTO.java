package mars.com.dto;

/**
 * Created by Harsh on 5/27/2017.
 */
public class SemestersDTO {
    String sem;
    String section;
    String sub;

    public String getSem() {
        return sem;
    }

    public void setSem(String sem) {
        this.sem = sem;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    @Override
    public String toString() {
        return "semesters: [sem=" + sem + ", section=" + section + ", sub=" + sub +
                "]";
    }
}
