package mars.com.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import mars.com.R;
import mars.com.dto.StudentMarksDTO;

/**
 * Created by Harsh on 4/21/2017.
 */
public class StudentMarksListAdapter extends RecyclerView.Adapter<StudentMarksListAdapter.StudentMarksViewHolder> {
    private LayoutInflater inflater;
    List<StudentMarksDTO> studentMarksDTOList = Collections.emptyList();
    private Context context;

    public interface OnItemClickListener {
        void onItemClick(StudentMarksDTO dto);
    }

    private StudentMarksListAdapter.OnItemClickListener listener;

    public StudentMarksListAdapter(Context context, List<StudentMarksDTO> studentMarksDTOList, OnItemClickListener listener) {
        this.context = context;
        this.studentMarksDTOList = studentMarksDTOList;
        inflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public StudentMarksViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.marks_list_custom_row, parent, false);
        StudentMarksListAdapter.StudentMarksViewHolder holder = new StudentMarksListAdapter.StudentMarksViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(StudentMarksViewHolder holder, int position) {
        holder.bind(studentMarksDTOList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return studentMarksDTOList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    class StudentMarksViewHolder extends RecyclerView.ViewHolder {

        TextView txtMarksSNo, txtMarksStudentName;
        EditText etMarksStudent;

        public StudentMarksViewHolder(View itemView) {
            super(itemView);
            txtMarksSNo = (TextView) itemView.findViewById(R.id.txtMarksSNo);
            txtMarksStudentName = (TextView) itemView.findViewById(R.id.txtMarksStudentName);
            etMarksStudent = (EditText) itemView.findViewById(R.id.etMarksStudent);
        }

        public void bind(final StudentMarksDTO studentMarksDTO, final StudentMarksListAdapter.OnItemClickListener listener) {
            txtMarksSNo.setText(String.valueOf(getPosition() + 1));
            txtMarksStudentName.setText(studentMarksDTO.getName());
            studentMarksDTO.setMarks(etMarksStudent.getText().toString());
            txtMarksSNo.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Whitney-Book-Bas.otf"));
            txtMarksStudentName.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Whitney-Book-Bas.otf"));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(studentMarksDTO);
                }
            });
            etMarksStudent.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Whitney-Book-Bas.otf"));

        }
    }
}
