package mars.com.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import mars.com.R;
import mars.com.ui.CustomTitle;
import mars.com.util.SessionManager;

/**
 * Created by Harsh on 4/15/2017.
 */
public class TeacherDashboardActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.txtTeacherDashboardInfo)
    TextView txtTeacherDashboardInfo;
    @Bind(R.id.listClasses)
    ListView listClasses;
    String listItem;
    final ArrayList<String> values = new ArrayList<String>();
    SessionManager sessionManager;
    SharedPreferences preferences;
    ProgressDialog dialog1;
    String classes;
    JSONArray jsonArrayClasses;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_dashboard);
        sessionManager = new SessionManager(this);
        preferences = getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(TeacherDashboardActivity.this, "  Home"));
        txtTeacherDashboardInfo.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Whitney-Book-Bas.otf"));

        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            if (bundle.containsKey("attendance") && bundle.getString("attendance").equals("attendance")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(TeacherDashboardActivity.this);
                LayoutInflater inflater = this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.success_dialog, null);
                builder.setView(dialogView);
                final AlertDialog alertDialog = builder.create();
                Button btnDone = (Button) dialogView.findViewById(R.id.btnDone);
                btnDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        }

        Bundle bundle1 = getIntent().getExtras();
        if (null != bundle1) {
            if (bundle1.containsKey("marks") && bundle1.getString("marks").equals("marks")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(TeacherDashboardActivity.this);
                LayoutInflater inflater = this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.success_dialog, null);
                builder.setView(dialogView);
                final AlertDialog alertDialog = builder.create();
                Button btnDone = (Button) dialogView.findViewById(R.id.btnDone);
                btnDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        }

        classes = preferences.getString("classes", null);
        try {
            jsonArrayClasses = new JSONArray(classes);
            for (int i = 0; i < jsonArrayClasses.length(); i++) {
                JSONObject jsonObject = jsonArrayClasses.getJSONObject(i);
                listItem = jsonObject.getString("sem").toUpperCase() + "-" + jsonObject.getString("section").toUpperCase() +
                        " (" + jsonObject.getString("sub").toUpperCase() + ") ";
                values.add(i, listItem);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.teacher_dashboard_list_custom_row, R.id.txtSemName, values) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    v = inflater.inflate(R.layout.teacher_dashboard_list_custom_row, null);
                }
                TextView txtSemName = (TextView) v.findViewById(R.id.txtSemName);
                txtSemName.setText(values.get(position));
                txtSemName.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Whitney-Book-Bas.otf"));
                return v;
            }
        };
        listClasses.setAdapter(adapter);

        listClasses.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                String item = parent.getAdapter().getItem(position).toString();
                new AlertDialog.Builder(TeacherDashboardActivity.this)
                        .setTitle("Choose One")
                        .setMessage("What do you want to do?")
                        .setPositiveButton("Update Attendance ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    JSONObject object = jsonArrayClasses.getJSONObject(position);
                                    String sub = object.getString("sub").toUpperCase();
                                    Intent intent = new Intent(TeacherDashboardActivity.this, UpdateAttendanceActivity.class);
                                    intent.putExtra("subject", sub);
                                    startActivity(intent);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        })
                        .setNegativeButton("Enter Marks", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    JSONObject object = jsonArrayClasses.getJSONObject(position);
                                    String sub = object.getString("sub").toUpperCase();
                                    Intent intent = new Intent(TeacherDashboardActivity.this, UpdateMarksActivity.class);
                                    intent.putExtra("subject", sub);
                                    startActivity(intent);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        })
                        .create()
                        .show();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.logout:
                logout();
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(TeacherDashboardActivity.this);
        builder.setTitle(getString(R.string.logout));
        builder.setMessage(getString(R.string.logout_message));

        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                        sessionManager.setTeacherLogin(false);
                        Map<String, ?> prefs = preferences.getAll();
                        for (Map.Entry<String, ?> prefToReset : prefs.entrySet()) {
                            if (!prefToReset.getKey().equals("exam")) {
                                preferences.edit().remove(prefToReset.getKey()).commit();
                            }
                        }
                        dialog.dismiss();
                        startActivity(new Intent(TeacherDashboardActivity.this, LoginModeActivity.class));
                        ActivityCompat.finishAffinity(TeacherDashboardActivity.this);
                    }
                });

        String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // negative button logic
                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }
}
