package mars.com.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import mars.com.R;
import mars.com.app.AppController;
import mars.com.constants.NetworkConstants;
import mars.com.ui.CustomTitle;
import mars.com.ui.SnackBar;

/**
 * Created by Harsh on 12/25/2016.
 */
public class MarksActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.txtFullMarks)
    TextView txtFullMarks;
    @Bind(R.id.txtSubMarks1)
    TextView txtSubMarks1;
    @Bind(R.id.txtSubMarks2)
    TextView txtSubMarks2;
    @Bind(R.id.txtSubMarks3)
    TextView txtSubMarks3;
    @Bind(R.id.txtCt1_one)
    TextView txtCt1One;
    @Bind(R.id.txtCt1_two)
    TextView txtCt1Two;
    @Bind(R.id.txtCt1_three)
    TextView txtCt1Three;
    @Bind(R.id.txtCt1_four)
    TextView txtCt1Four;
    @Bind(R.id.txtCt2_one)
    TextView txtCt2One;
    @Bind(R.id.txtCt2_two)
    TextView txtCt2Two;
    @Bind(R.id.txtCt2_three)
    TextView txtCt2Three;
    @Bind(R.id.txtCt2_four)
    TextView txtCt2Four;
    @Bind(R.id.txtCt3_one)
    TextView txtCt3One;
    @Bind(R.id.txtCt3_two)
    TextView txtCt3Two;
    @Bind(R.id.txtCt3_three)
    TextView txtCt3Three;
    @Bind(R.id.txtCt3_four)
    TextView txtCt3Four;
    SharedPreferences preferences;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marks);
        preferences = getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(MarksActivity.this, "Your Marks :"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Whitney-Semibold-Bas.otf");
        txtFullMarks.setTypeface(typeface);
        txtSubMarks1.setTypeface(typeface);
        txtSubMarks2.setTypeface(typeface);
        txtSubMarks3.setTypeface(typeface);
        txtCt1One.setTypeface(typeface);
        txtCt1Two.setTypeface(typeface);
        txtCt1Three.setTypeface(typeface);
        txtCt1Four.setTypeface(typeface);
        txtCt2One.setTypeface(typeface);
        txtCt2Two.setTypeface(typeface);
        txtCt2Three.setTypeface(typeface);
        txtCt2Four.setTypeface(typeface);
        txtCt3One.setTypeface(typeface);
        txtCt3Two.setTypeface(typeface);
        txtCt3Three.setTypeface(typeface);
        txtCt3Four.setTypeface(typeface);
        txtCt1One.setVisibility(View.INVISIBLE);
        txtCt1Two.setVisibility(View.INVISIBLE);
        txtCt1Three.setVisibility(View.INVISIBLE);
        txtCt1Four.setVisibility(View.INVISIBLE);
        txtCt2One.setVisibility(View.INVISIBLE);
        txtCt2Two.setVisibility(View.INVISIBLE);
        txtCt2Three.setVisibility(View.INVISIBLE);
        txtCt2Four.setVisibility(View.INVISIBLE);
        txtCt3One.setVisibility(View.INVISIBLE);
        txtCt3Two.setVisibility(View.INVISIBLE);
        txtCt3Three.setVisibility(View.INVISIBLE);
        txtCt3Four.setVisibility(View.INVISIBLE);
        fetchData();
    }

    private void fetchData() {
        final ProgressDialog progressDialog = ProgressDialog.show(MarksActivity.this, "", "Fetching Data...", false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstants.GET_STUDENT_MARKS_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.i("Response::", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("marks");
                    if (jsonArray.length() != 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            if (jsonArray.getJSONObject(i).getString("examination").equals("CT-1 Examination")) {
                                if (jsonArray.getJSONObject(i).getString("subject").equals("NCS-801")) {
                                    String marks = jsonArray.getJSONObject(i).getString("marks");
                                    txtCt1One.setVisibility(View.VISIBLE);
                                    txtCt1One.setText("NCS-801 : " + marks);
                                }
                                if (jsonArray.getJSONObject(i).getString("subject").equals("NCS-081")) {
                                    String marks = jsonArray.getJSONObject(i).getString("marks");
                                    txtCt1Two.setVisibility(View.VISIBLE);
                                    txtCt1Two.setText("NCS-081 : " + marks);
                                }
                                if (jsonArray.getJSONObject(i).getString("subject").equals("NCS-085")) {
                                    String marks = jsonArray.getJSONObject(i).getString("marks");
                                    txtCt1Three.setVisibility(View.VISIBLE);
                                    txtCt1Three.setText("NCS-085 : " + marks);
                                }
                                if (jsonArray.getJSONObject(i).getString("subject").equals("EOE-801")) {
                                    String marks = jsonArray.getJSONObject(i).getString("marks");
                                    txtCt1Four.setVisibility(View.VISIBLE);
                                    txtCt1Four.setText("EOE-801 : " + marks);
                                }
                            }
                            if (jsonArray.getJSONObject(i).getString("examination").equals("CT-2 Examination")) {
                                if (jsonArray.getJSONObject(i).getString("subject").equals("NCS-801")) {
                                    String marks = jsonArray.getJSONObject(i).getString("marks");
                                    txtCt2One.setVisibility(View.VISIBLE);
                                    txtCt2One.setText("NCS-801 : " + marks);
                                }
                                if (jsonArray.getJSONObject(i).getString("subject").equals("NCS-081")) {
                                    String marks = jsonArray.getJSONObject(i).getString("marks");
                                    txtCt2Two.setVisibility(View.VISIBLE);
                                    txtCt2Two.setText("NCS-081 : " + marks);
                                }
                                if (jsonArray.getJSONObject(i).getString("subject").equals("NCS-085")) {
                                    String marks = jsonArray.getJSONObject(i).getString("marks");
                                    txtCt2Three.setVisibility(View.VISIBLE);
                                    txtCt2Three.setText("NCS-085 : " + marks);
                                }
                                if (jsonArray.getJSONObject(i).getString("subject").equals("EOE-801")) {
                                    String marks = jsonArray.getJSONObject(i).getString("marks");
                                    txtCt2Four.setVisibility(View.VISIBLE);
                                    txtCt2Four.setText("EOE-801 : " + marks);
                                }
                            }
                            if (jsonArray.getJSONObject(i).getString("examination").equals("Pre-Semester Examination")) {
                                if (jsonArray.getJSONObject(i).getString("subject").equals("NCS-801")) {
                                    String marks = jsonArray.getJSONObject(i).getString("marks");
                                    txtCt3One.setVisibility(View.VISIBLE);
                                    txtCt3One.setText("NCS-801 : " + marks);
                                }
                                if (jsonArray.getJSONObject(i).getString("subject").equals("NCS-081")) {
                                    String marks = jsonArray.getJSONObject(i).getString("marks");
                                    txtCt3Two.setVisibility(View.VISIBLE);
                                    txtCt3Two.setText("NCS-081 : " + marks);
                                }
                                if (jsonArray.getJSONObject(i).getString("subject").equals("NCS-085")) {
                                    String marks = jsonArray.getJSONObject(i).getString("marks");
                                    txtCt3Three.setVisibility(View.VISIBLE);
                                    txtCt3Three.setText("NCS-085 : " + marks);
                                }
                                if (jsonArray.getJSONObject(i).getString("subject").equals("EOE-801")) {
                                    String marks = jsonArray.getJSONObject(i).getString("marks");
                                    txtCt3Four.setVisibility(View.VISIBLE);
                                    txtCt3Four.setText("EOE-801 : " + marks);
                                }
                            }
                        }
                    } else {
                        SnackBar.show(MarksActivity.this, getString(R.string.no_data_error));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("Error:::", error.toString());
                SnackBar.show(MarksActivity.this, getString(R.string.volley_error_message));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("rollno", preferences.getString("rollno", null));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
