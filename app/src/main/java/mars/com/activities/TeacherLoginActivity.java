package mars.com.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mars.com.R;
import mars.com.app.AppController;
import mars.com.constants.NetworkConstants;
import mars.com.ui.CustomTitle;
import mars.com.ui.SnackBar;
import mars.com.ui.button.ButtonPlus;
import mars.com.util.NetworkCheck;
import mars.com.util.SessionManager;

/**
 * Created by Harsh on 4/15/2017.
 */
public class TeacherLoginActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.etTeacherId)
    EditText etTeacherId;
    @Bind(R.id.etTeacherPassword)
    EditText etTeacherPassword;
    @Bind(R.id.txtTeacherLoginMessage)
    TextView txtTeacherLoginMessage;
    @Bind(R.id.txtTeacherLoginDescMessage)
    TextView txtTeacherLoginDescMessage;
    @Bind(R.id.btnSubmit)
    ButtonPlus btnSubmit;
    SharedPreferences preferences;
    SessionManager sessionManager;

    @OnClick(R.id.btnSubmit)
    void login() {
        if (etTeacherId.getText().toString().equals("") || etTeacherPassword.getText().toString().equals("")) {
            SnackBar.show(this, getString(R.string.no_text));
            return;
        } else if (!NetworkCheck.isNetworkAvailable(this)) {
            SnackBar.show(this, getString(R.string.no_internet));
            return;
        } else {
            teacherLogin(etTeacherId.getText().toString(), etTeacherPassword.getText().toString());
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_login);
        sessionManager = new SessionManager(this);
        preferences = getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(TeacherLoginActivity.this, "Enter your Faculty Id"));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        txtTeacherLoginMessage.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Whitney-Semibold-Bas.otf"));
        txtTeacherLoginDescMessage.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Whitney-Semibold-Bas.otf"));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnSubmit.setBackgroundResource(R.drawable.ripple_rounded);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void teacherLogin(final String id, final String password) {
        final ProgressDialog dialog = ProgressDialog.show(TeacherLoginActivity.this, "", "Loading...", true);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstants.FACULTY_ACCESS_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                Log.i("Login Response:::", response);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean error = jsonObject.getBoolean("error");

                    if (!error) {
                        sessionManager.setTeacherLogin(true);
                        JSONArray jsonArray = jsonObject.getJSONArray("semesters");
                        Intent intent = new Intent(TeacherLoginActivity.this, TeacherDashboardActivity.class);
                        preferences.edit().putString("classes", jsonArray.toString()).commit();
                        startActivity(intent);
                        ActivityCompat.finishAffinity(TeacherLoginActivity.this);
                    } else {
                        SnackBar.show(TeacherLoginActivity.this, getString(R.string.invalid_login));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e("Error:::", error.toString());
                SnackBar.show(TeacherLoginActivity.this, getString(R.string.volley_error_message));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", id);
                params.put("password", password);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
