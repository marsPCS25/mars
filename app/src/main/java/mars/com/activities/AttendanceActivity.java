package mars.com.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import mars.com.R;
import mars.com.app.AppController;
import mars.com.constants.NetworkConstants;
import mars.com.ui.CustomTitle;
import mars.com.ui.SnackBar;

/**
 * Created by Harsh on 12/25/2016.
 */
public class AttendanceActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.txtFullAttendance)
    TextView txtFullAttendance;
    @Bind(R.id.txtSubAttendance)
    TextView txtSubAttendance;
    @Bind(R.id.txtFirstSub)
    TextView txtFirstSub;
    @Bind(R.id.txtSecondSub)
    TextView txtSecondSub;
    @Bind(R.id.txtThirdSub)
    TextView txtThirdSub;
    @Bind(R.id.txtFourthSub)
    TextView txtFourthSub;
    SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        preferences = getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        populate();
    }


    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(AttendanceActivity.this, "Your Attendance :"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Whitney-Semibold-Bas.otf");
        txtFullAttendance.setTypeface(typeface);
        txtSubAttendance.setTypeface(typeface);
        txtFirstSub.setTypeface(typeface);
        txtSecondSub.setTypeface(typeface);
        txtThirdSub.setTypeface(typeface);
        txtFourthSub.setTypeface(typeface);
        txtFirstSub.setVisibility(View.INVISIBLE);
        txtSecondSub.setVisibility(View.INVISIBLE);
        txtThirdSub.setVisibility(View.INVISIBLE);
        txtFourthSub.setVisibility(View.INVISIBLE);
        fetchData();
    }

    private void fetchData() {
        final ProgressDialog progressDialog = ProgressDialog.show(AttendanceActivity.this, "", "Fetching Data...", false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstants.GET_STUDENT_ATTENDANCE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                int pFirst = 0, aFirst = 0, totFirst = 0;
                int pSecond = 0, aSecond = 0, totSecond = 0;
                int pThird = 0, aThird = 0, totThird = 0;
                int pFourth = 0, aFourth = 0, totFourth = 0;
                progressDialog.dismiss();
                Log.i("Response::", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("attendance");
                    if (jsonArray.length() != 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            if (jsonArray.getJSONObject(i).getString("subject").equals("NCS-801")) {
                                if (jsonArray.getJSONObject(i).getString("status").equals("A")) {
                                    aFirst++;
                                } else {
                                    pFirst++;
                                }
                            } else if (jsonArray.getJSONObject(i).getString("subject").equals("NCS-081")) {
                                if (jsonArray.getJSONObject(i).getString("status").equals("A")) {
                                    aSecond++;
                                } else {
                                    pSecond++;
                                }
                            } else if (jsonArray.getJSONObject(i).getString("subject").equals("NCS-085")) {
                                if (jsonArray.getJSONObject(i).getString("status").equals("A")) {
                                    aThird++;
                                } else {
                                    pThird++;
                                }
                            } else if (jsonArray.getJSONObject(i).getString("subject").equals("EOE-801")) {
                                if (jsonArray.getJSONObject(i).getString("status").equals("A")) {
                                    aFourth++;
                                } else {
                                    pFourth++;
                                }
                            }
                        }

                        if (pFirst != 0 || aFirst != 0) {
                            totFirst = (pFirst * 100) / (aFirst + pFirst);
                            txtFirstSub.setVisibility(View.VISIBLE);
                            if (totFirst <= 50){
                                txtFirstSub.setTextColor(getResources().getColor(R.color.low_aatendance));
                            } else if (totFirst > 50 && totFirst < 75){
                                txtFirstSub.setTextColor(getResources().getColor(R.color.medium_attendance));
                            } else {
                                txtFirstSub.setTextColor(getResources().getColor(R.color.good_attendance));
                            }
                            txtFirstSub.setText("NCS - 801 : " + String.valueOf(totFirst) + "%");
                        }
                        if (pSecond != 0 || aSecond != 0) {
                            totSecond = (pSecond * 100) / (aSecond + pSecond);
                            if (totSecond <= 50){
                                txtSecondSub.setTextColor(getResources().getColor(R.color.low_aatendance));
                            } else if (totFirst > 50 && totFirst < 75){
                                txtSecondSub.setTextColor(getResources().getColor(R.color.medium_attendance));
                            } else {
                                txtSecondSub.setTextColor(getResources().getColor(R.color.good_attendance));
                            }
                            txtSecondSub.setVisibility(View.VISIBLE);
                            txtSecondSub.setText("NCS - 081 : " + String.valueOf(totSecond) + "%");
                        }
                        if (pThird != 0 || aThird != 0) {
                            totThird = (pThird * 100) / (aThird + pThird);
                            if (totThird <= 50){
                                txtThirdSub.setTextColor(getResources().getColor(R.color.low_aatendance));
                            } else if (totFirst > 50 && totFirst < 75){
                                txtThirdSub.setTextColor(getResources().getColor(R.color.medium_attendance));
                            } else {
                                txtThirdSub.setTextColor(getResources().getColor(R.color.good_attendance));
                            }
                            txtThirdSub.setVisibility(View.VISIBLE);
                            txtThirdSub.setText("NCS - 085 : " + String.valueOf(totThird) + "%");
                        }
                        if (pFourth != 0 || aFourth != 0) {
                            totFourth = (pFourth * 100) / (aFourth + pFourth);
                            if (totFourth <= 50){
                                txtFourthSub.setTextColor(getResources().getColor(R.color.low_aatendance));
                            } else if (totFirst > 50 && totFirst < 75){
                                txtFourthSub.setTextColor(getResources().getColor(R.color.medium_attendance));
                            } else {
                                txtFourthSub.setTextColor(getResources().getColor(R.color.good_attendance));
                            }
                            txtFourthSub.setVisibility(View.VISIBLE);
                            txtFourthSub.setText("EOE - 801 : " + String.valueOf(totFourth) + "%");
                        }
                        int total = (totFirst + totSecond + totThird + totFourth) / 4;
                        txtFullAttendance.setText("Total Attendance is : " + String.valueOf(total) + "%");
                    } else {
                        SnackBar.show(AttendanceActivity.this, getString(R.string.no_data_error));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("Error::", error.toString());
                SnackBar.show(AttendanceActivity.this, getString(R.string.volley_error_message));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("rollno", preferences.getString("rollno", null));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
