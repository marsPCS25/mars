package mars.com.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;
import mars.com.R;
import mars.com.adapters.StudentMarksListAdapter;
import mars.com.app.AppController;
import mars.com.constants.NetworkConstants;
import mars.com.dto.StudentMarksDTO;
import mars.com.ui.CustomTitle;
import mars.com.ui.SnackBar;
import mars.com.ui.button.ButtonPlus;

/**
 * Created by Harsh on 4/21/2017.
 */
public class UpdateMarksActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.spinnerExam)
    MaterialSpinner spinnerExam;
    @Bind(R.id.listStudentMarks)
    RecyclerView listStudentMarks;
    @Bind(R.id.btnSubmitMarks)
    ButtonPlus btnSubmitMarks;
    final List<StudentMarksDTO> studentMarksDTOList = new ArrayList<>();
    StudentMarksListAdapter adapter;
    private String[] exams;
    SharedPreferences preferences;
    ProgressDialog dialog1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_marks);
        preferences = getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(UpdateMarksActivity.this, "List Of Students"));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final String subject = getIntent().getStringExtra("subject");
        exams = getApplication().getResources().getStringArray(R.array.exams);
        ArrayAdapter<String> adapterSpinner = new ArrayAdapter<String>(UpdateMarksActivity.this, android.R.layout.simple_spinner_item, exams);
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerExam.setAdapter(adapterSpinner);
        fetchStudentList();
        adapter = new StudentMarksListAdapter(UpdateMarksActivity.this, studentMarksDTOList, new StudentMarksListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(StudentMarksDTO dto) {
                Toast.makeText(UpdateMarksActivity.this, dto.getName(), Toast.LENGTH_SHORT).show();
            }
        });
        listStudentMarks.setAdapter(adapter);
        listStudentMarks.setLayoutManager(new LinearLayoutManager(UpdateMarksActivity.this));

        btnSubmitMarks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spinnerExam.getSelectedItem().equals("Select Exam")) {
                    SnackBar.show(UpdateMarksActivity.this, "Please Select Examination");
                    return;
                }

                if (preferences.getString("exam", null) != null && preferences.getString("exam", null).equals(spinnerExam.getSelectedItem().toString() + subject)) {
                    SnackBar.show(UpdateMarksActivity.this, "Marks have already been uploaded!!!");
                    return;
                }

                for (int i = 0; i < studentMarksDTOList.size(); i++) {
                    if (studentMarksDTOList.get(i).getMarks() == null) {
                        SnackBar.show(UpdateMarksActivity.this, "Please Enter the Marks of All Students");
                        return;
                    }
                }

                new AlertDialog.Builder(UpdateMarksActivity.this)
                        .setTitle("Confirmation")
                        .setMessage("Are you sure to submit the marks?")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                JSONArray jsonArray = new JSONArray();
                                JSONObject jsonObjectNew = new JSONObject();
                                JSONArray jsonArrayMarks = new JSONArray();
                                for (int j = 0; j < studentMarksDTOList.size(); j++) {
                                    JSONObject jsonObject = new JSONObject();
                                    JSONObject jsonObjectMarks = new JSONObject();
                                    try {
                                        jsonObject.put("name", studentMarksDTOList.get(j).getName());
                                        jsonObject.put("rollNumber", studentMarksDTOList.get(j).getRollno());
                                        jsonObject.put("semester", studentMarksDTOList.get(j).getSemester());
                                        jsonObject.put("section", studentMarksDTOList.get(j).getSection());
                                        jsonObjectMarks.put("subject", subject);
                                        jsonObjectMarks.put("examination", spinnerExam.getSelectedItem().toString());
                                        jsonObjectMarks.put("marks", studentMarksDTOList.get(j).getMarks());
                                        jsonArrayMarks.put(jsonObjectMarks);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        jsonObject.put("marks", jsonArrayMarks.get(j));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    jsonArray.put(jsonObject);
                                }
                                try {
                                    jsonObjectNew.put("students", jsonArray);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                String tag_str_req = "req_sync";
                                final ProgressDialog progressDialog = ProgressDialog.show(UpdateMarksActivity.this, "", "Loading...", false);
                                JsonObjectRequest request = new JsonObjectRequest(NetworkConstants.MARKS_UPLOAD_URL, jsonObjectNew, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        progressDialog.dismiss();
                                        Log.i("Response:::", response.toString());
                                        try {
                                            String success = response.getString("response");
                                            if (success.equals("Success")) {
                                                preferences.edit().putString("exam", spinnerExam.getSelectedItem().toString() + subject).commit();
                                                Intent intent = new Intent(UpdateMarksActivity.this, TeacherDashboardActivity.class);
                                                Bundle bundle = new Bundle();
                                                bundle.putString("marks", "marks");
                                                intent.putExtras(bundle);
                                                startActivity(intent);
                                                finish();
                                            } else {
                                                progressDialog.dismiss();
                                                SnackBar.show(UpdateMarksActivity.this, getString(R.string.error_message));
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        progressDialog.dismiss();
                                        Log.e("Error:::", error.toString());
                                        SnackBar.show(UpdateMarksActivity.this, getString(R.string.volley_error_message));
                                    }
                                });
                                AppController.getInstance().addToRequestQueue(request, tag_str_req);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
/*

    public List<StudentMarksDTO> getData() {
        StudentMarksDTO dto;
        String classList = preferences.getString("classList", null);
        try {
            JSONArray jsonArray = new JSONArray(classList);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                dto = new StudentMarksDTO();
                dto.setName(jsonObject.getString("name"));
                dto.setRollno(jsonObject.getString("rollno"));
                dto.setSemester(jsonObject.getString("semester"));
                dto.setSection(jsonObject.getString("section"));
                studentMarksDTOList.add(dto);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return studentMarksDTOList;
    }
*/


    private List<StudentMarksDTO> fetchStudentList() {
        dialog1 = ProgressDialog.show(UpdateMarksActivity.this, "", "Getting Class List...", false);
        StringRequest request = new StringRequest(Request.Method.POST, NetworkConstants.GET_CLASS_LIST_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("Response:::", response);
                StudentMarksDTO dto;
                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONArray jsonArray = jsonObjectResponse.getJSONArray("students");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        dto = new StudentMarksDTO();
                        dto.setName(jsonObject.getString("name"));
                        dto.setRollno(jsonObject.getString("rollNumber"));
                        dto.setSemester(jsonObject.getString("semester"));
                        dto.setSection(jsonObject.getString("section"));
                        studentMarksDTOList.add(dto);
                    }
                    dialog1.dismiss();
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog1.dismiss();
                Log.e("Error:::", error.toString());
                SnackBar.show(UpdateMarksActivity.this, getString(R.string.error_message));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("classlist", "8-A");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
        return studentMarksDTOList;
    }
}
