package mars.com.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mars.com.R;
import mars.com.app.AppController;
import mars.com.constants.NetworkConstants;
import mars.com.ui.CustomTitle;
import mars.com.ui.SnackBar;
import mars.com.ui.button.ButtonPlus;
import mars.com.util.NetworkCheck;
import mars.com.util.SessionManager;

/**
 * Created by Harsh on 3/16/2016.
 */
public class LoginActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.etRollNumber)
    android.widget.EditText etRollNumber;
    @Bind(R.id.btnSubmit)
    ButtonPlus btnSubmit;
    @Bind(R.id.txtLoginMessage)
    TextView txtLoginMessage;
    @Bind(R.id.txtLoginDescMessage)
    TextView txtLoginDescMessage;
    SessionManager sessionManager;
    SharedPreferences preferences;

    @OnClick(R.id.btnSubmit)
    void enter() {
        if (etRollNumber.getText().toString().equals("")) {
            SnackBar.show(this, getString(R.string.no_text));
            return;
        } else if (!NetworkCheck.isNetworkAvailable(LoginActivity.this)) {
            SnackBar.show(LoginActivity.this, getString(R.string.no_internet));
            return;
        } else {
            studentLogin(etRollNumber.getText().toString());
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sessionManager = new SessionManager(this);
        preferences = getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnSubmit.setBackgroundResource(R.drawable.ripple_rounded);
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(this, getString(R.string.login)));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtLoginMessage.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Whitney-Semibold-Bas.otf"));
        txtLoginDescMessage.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Whitney-Semibold-Bas.otf"));
        //    txtCollegeCode.setTypeface(typeface);
    }


    private void studentLogin(final String rollno) {
        final String TAG = "student_login_req";
        final ProgressDialog dialog = ProgressDialog.show(LoginActivity.this, "", "Loading...", true);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstants.STUDENT_ACCESS_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                Log.i(TAG, response);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean error = jsonObject.getBoolean("error");

                    if (!error) {
                        sessionManager.setStudentLogin(true);
                        Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                        preferences.edit().putString("name", jsonObject.getString("name")).commit();
                        preferences.edit().putString("rollno", jsonObject.getString("rollno")).commit();
                        preferences.edit().putString("semester", jsonObject.getString("semester")).commit();
                        preferences.edit().putString("section", jsonObject.getString("section")).commit();
                        startActivity(intent);
                        ActivityCompat.finishAffinity(LoginActivity.this);
                    } else {
                        SnackBar.show(LoginActivity.this, getString(R.string.invalid_login));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e(TAG, error.toString());
                SnackBar.show(LoginActivity.this, getString(R.string.volley_error_message));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("rollno", rollno);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest, TAG);
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

