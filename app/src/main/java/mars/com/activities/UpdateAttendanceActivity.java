package mars.com.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import mars.com.R;
import mars.com.adapters.StudentListAdapter;
import mars.com.app.AppController;
import mars.com.constants.NetworkConstants;
import mars.com.dto.StudentAttendanceDTO;
import mars.com.ui.CustomTitle;
import mars.com.ui.SnackBar;
import mars.com.ui.button.ButtonPlus;

/**
 * Created by Harsh on 4/17/2017.
 */
public class UpdateAttendanceActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.txtDate)
    TextView txtDate;
    @Bind(R.id.listStudents)
    RecyclerView listStudents;
    @Bind(R.id.btnSubmitAttendance)
    ButtonPlus btnSubmitAttendance;
    final List<StudentAttendanceDTO> studentAttendanceDTOList = new ArrayList<>();
    StudentListAdapter adapter;
    SharedPreferences preferences;
    ProgressDialog dialog1;
    int count = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_attendance);
        preferences = getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(UpdateAttendanceActivity.this, "List Of Students"));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final String subject = getIntent().getStringExtra("subject");

        long date = System.currentTimeMillis();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy");
        String dateString = simpleDateFormat.format(date);
        txtDate.setText(dateString);
        txtDate.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Whitney-Book-Bas.otf"));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnSubmitAttendance.setBackgroundResource(R.drawable.ripple);
        }
        fetchStudentList();
        adapter = new StudentListAdapter(UpdateAttendanceActivity.this, studentAttendanceDTOList, new StudentListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(StudentAttendanceDTO dto) {
                Toast.makeText(UpdateAttendanceActivity.this, dto.getStatus(), Toast.LENGTH_SHORT).show();
            }
        });

        listStudents.setAdapter(adapter);
        listStudents.invalidate();
        listStudents.setLayoutManager(new LinearLayoutManager(UpdateAttendanceActivity.this));

        btnSubmitAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < studentAttendanceDTOList.size(); i++) {
                    if (studentAttendanceDTOList.get(i).getStatus().equals("A"))
                        count++;
                }

                new AlertDialog.Builder(UpdateAttendanceActivity.this)
                        .setTitle("Total Absent")
                        .setMessage("No. of Student Absent: " + count)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                JSONArray jsonArray = new JSONArray();
                                JSONObject jsonObjectNew = new JSONObject();
                                JSONArray jsonArrayAttendance = new JSONArray();
                                for (int j = 0; j < studentAttendanceDTOList.size(); j++) {
                                    JSONObject jsonObject = new JSONObject();
                                    JSONObject jsonObjectAttendance = new JSONObject();
                                    try {
                                        Log.i("j::", String.valueOf(j));
                                        jsonObject.put("name", studentAttendanceDTOList.get(j).getName());
                                        jsonObject.put("rollNumber", studentAttendanceDTOList.get(j).getRollno());
                                        jsonObject.put("semester", studentAttendanceDTOList.get(j).getSemester());
                                        jsonObject.put("section", studentAttendanceDTOList.get(j).getSection());
                                        jsonObjectAttendance.put("subject", subject);
                                        jsonObjectAttendance.put("date", txtDate.getText().toString());
                                        jsonObjectAttendance.put("status", studentAttendanceDTOList.get(j).getStatus());
                                        jsonArrayAttendance.put(jsonObjectAttendance);
                                        //Log.i("Status::", studentAttendanceDTOList.get(j).getStatus());
                                        //Log.i("att:::", jsonArrayAttendance.toString());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        jsonObject.put("attendance", jsonArrayAttendance.get(j));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    jsonArray.put(jsonObject);
                                }
                                try {
                                    jsonObjectNew.put("students", jsonArray);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                String tag_str_req = "req_sync";
                                final ProgressDialog progressDialog = ProgressDialog.show(UpdateAttendanceActivity.this, "", "Loading...", false);
                                JsonObjectRequest request = new JsonObjectRequest(NetworkConstants.ATTENDANCE_UPLOAD_URL, jsonObjectNew, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        progressDialog.dismiss();
                                        Log.i("Attendance Response:::", response.toString());
                                        try {
                                            String success = response.getString("response");
                                            if (success.equals("Success")) {
                                                Intent intent = new Intent(UpdateAttendanceActivity.this, TeacherDashboardActivity.class);
                                                Bundle bundle = new Bundle();
                                                bundle.putString("attendance", "attendance");
                                                intent.putExtras(bundle);
                                                startActivity(intent);
                                                finish();
                                            } else {
                                                progressDialog.dismiss();
                                                SnackBar.show(UpdateAttendanceActivity.this, getString(R.string.error_message));
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        progressDialog.dismiss();
                                        Log.e("Error:::", error.toString());
                                        SnackBar.show(UpdateAttendanceActivity.this, getString(R.string.volley_error_message));
                                    }
                                });
                                AppController.getInstance().addToRequestQueue(request, tag_str_req);
                            }
                        })
                        .setNegativeButton("Review Before Exit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();
                count = 0;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
/*
    public List<StudentAttendanceDTO> getData() {
        StudentAttendanceDTO dto;
        String classList = preferences.getString("classList", null);
        try {
            JSONArray jsonArray = new JSONArray(classList);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                dto = new StudentAttendanceDTO();
                dto.setName(jsonObject.getString("name"));
                dto.setRollno(jsonObject.getString("rollno"));
                dto.setSemester(jsonObject.getString("semester"));
                dto.setSection(jsonObject.getString("section"));
                dto.setStatus("P");
                studentAttendanceDTOList.add(dto);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return studentAttendanceDTOList;
    }*/

    private List<StudentAttendanceDTO> fetchStudentList() {
        dialog1 = ProgressDialog.show(UpdateAttendanceActivity.this, "", "Getting Class List...", false);
        StringRequest request = new StringRequest(Request.Method.POST, NetworkConstants.GET_CLASS_LIST_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("Response:::", response);
                StudentAttendanceDTO dto;
                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONArray jsonArray = jsonObjectResponse.getJSONArray("students");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        dto = new StudentAttendanceDTO();
                        dto.setName(jsonObject.getString("name"));
                        dto.setRollno(jsonObject.getString("rollNumber"));
                        dto.setSemester(jsonObject.getString("semester"));
                        dto.setSection(jsonObject.getString("section"));
                        dto.setStatus("P");
                        studentAttendanceDTOList.add(dto);
                    }
                    dialog1.dismiss();
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog1.dismiss();
                Log.e("Error:::", error.toString());
                SnackBar.show(UpdateAttendanceActivity.this, getString(R.string.error_message));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("classlist", "8-A");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
        return studentAttendanceDTOList;
    }
}
